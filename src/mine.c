
/*H**********
 * 
 * Filename: mine.c
 * Author :	Preben Vangberg
 * Date	: 05.10.2018
 *
 * Description:
 * 		Simple CLI Mineweeper game.
 *
 *H*/

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#define MAPSIZE 16

int numberOfMines = 0;
int numberOfFlags = 0;
int playerPosX;
int playerPosY;
char map[MAPSIZE][MAPSIZE] = {'#'};
int mines[MAPSIZE][MAPSIZE] = { 0 };
int running = 1;

int checkStatus()
{
	for(int x = 0; x < MAPSIZE; x++)
		for(int y = 0; y < MAPSIZE; y++)
			if(mines[x][y] == 1 && map[x][y] != '$')
				return 0;
	return 1;
}

void printChar(char c)
{
	if(c == '#')printf("\x1B[0m");
	else if(c == '$')printf("\x1B[0m");
	else if(c == '1')printf("\x1B[34m");
	else if(c == '2')printf("\x1B[32m");
	else if(c == '3')printf("\x1B[33m");
	else printf("\x1B[31m");
	printf("%c\x1B[0m", c);
}

int getch()
{
	int c;
	struct termios oldt;
	struct termios newt;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO); 
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	c = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	return c;
}

void printLine()
{
	printf("+");
	for(int i = 0; i < MAPSIZE; i++){
		printf("-+");
	}
	printf("\n");
}

void printMap()
{
	for(int y = 0; y < MAPSIZE; y++){
		printLine();
		for(int x = 0; x < MAPSIZE; x++){
			if(x == playerPosX && y == playerPosY){
				if(map[x][y] == ' ')
					printf("|%s%c%s", "\x1B[35m", 'p', "\x1B[0m");
				else
					printf("|%s%c%s","\x1B[35m",map[x][y],"\x1B[0m");
			} else {
				printf("|");
				printChar(map[x][y]);				
			}
		}
		printf("|\n");
	}
	printLine();
}

void printMines()
{
	for(int y = 0; y < MAPSIZE; y++){
		for(int x = 0; x < MAPSIZE; x++){
			printf("%d ", mines[x][y]);
		}
		printf("\n");
	}
}

void revealCharAt(int posx, int posy)
{
	if(map[posx][posy] == '$')
		numberOfFlags--;
	if(mines[posx][posy] == 1){
		map[posx][posy] = 'X';
		running = 0;
		return;
	}
	int minesFound = 0;
	for(int x = -1; x < 2; x++)
		for(int y = -1; y < 2; y++)
			if(posx + x >= 0 && posy + y >= 0)
				if(posx + x < MAPSIZE && posy + y < MAPSIZE)
					if(mines[posx + x][posy + y] == 1)
						minesFound++;
	if(minesFound != 0){			
		map[posx][posy] = minesFound + '0';
	} else {
		map[posx][posy] = ' ';
		for(int x = -1; x < 2; x++)
			for(int y = -1; y < 2; y++)
				if(posx + x >= 0 && posy + y >= 0)
					if(posx + x < MAPSIZE && posy + y < MAPSIZE)
						if(map[posx + x][posy + y] == '#')
							revealCharAt(posx + x, posy + y);
	}
}

void placeFlag(int posx, int posy){
	if(map[posx][posy] == '$'){
		map[posx][posy] = '#';
		numberOfFlags--;
	} else if(map[posx][posy] == '#') {
		map[posx][posy] = '$';
		numberOfFlags++;
	}
}

int gameLoop()
{
	running = 1;
	while(running){
		system("clear");
		printf("         MINESWEEPER\n");
		printMap();
		printf("Mines:%d\tMines left:%d\n", numberOfMines, (numberOfMines - numberOfFlags));

		char c = getch();

		if(c == 'q') return -1;
		else if(c == 'r')revealCharAt(playerPosX, playerPosY);
		else if(c == 'f')placeFlag(playerPosX, playerPosY);
		else if(c == 'h'|| c == 'a')playerPosX--;
		else if(c == 'j'|| c == 's')playerPosY++;
		else if(c == 'k'|| c == 'w')playerPosY--;
		else if(c == 'l'|| c == 'd')playerPosX++;
		else if(c == 'c'){
			int result = checkStatus();
			if(result == 1) return 1;
		}


		if(playerPosX < 0)playerPosX = 0;
		if(playerPosY < 0)playerPosY = 0;
		if(playerPosX >= MAPSIZE)playerPosX = MAPSIZE - 1;
		if(playerPosY >= MAPSIZE)playerPosY = MAPSIZE - 1;
	}
}

void revealAll()
{
	for(int x = 0; x < MAPSIZE; x++)
		for(int y = 0; y < MAPSIZE; y++)
			revealCharAt(x, y);
}

int gameover(int exitCode)
{
	revealAll();
	system("clear");
	printMap();
	if(exitCode <= 0){
		printf("GAME OVER!\n");
		if(exitCode == -1)
			exit(0);
	}
	if(exitCode == 1)
		printf("YOU WON!\n");
	return exitCode;
}

void init()
{	
	playerPosX = 0;
	playerPosY = 0;
	numberOfMines = 0;
	numberOfFlags = 0;
	srand(time(NULL));
	for(int x = 0; x < MAPSIZE; x++){
		for(int y = 0; y < MAPSIZE; y++){
			map[x][y] = '#';
			int r = rand() % 5;
			if(r == 0) {
				mines[x][y] = 1;
				numberOfMines++;
			} else {
				mines[x][y] = 0;
			}
		}
	}	
}
int main()
{
	while(1){
		init();
		gameover(gameLoop());
		printf("Press any button to play again!");
		getch();
	}
	return 0;
}





