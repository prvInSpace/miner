# Miner; CLI Minesweeper

A simple minesweeper game made in C. Just clone and run the setup.
To start the game simply type "miner" in the terminal

## Controls
Check if Finished:	c \
Place a flag: f or m \
Reveal block:	r     
 
Quit:	q\
Up:		k or w\
Down:	j or s\	 
Left:	l or a\
Right:	h or d\

## Credits
Made by Preben Vangberg - 2018
