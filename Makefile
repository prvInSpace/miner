OS = Linux

VERSION = 0.0.1
CC	= /usr/bin/gcc

TARGET := bin/main

SRCDIR := src
OBJDIR := obj


SRCS := $(shell find $(SRCDIR) -name "*.c")
OBJS := $(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

$(TARGET): $(OBJS)
	$(CC) -o $@ $^

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CC) -Wall $(@:$(OBJDIR)/%.o) -o $@ -c $<

clean: 
	rm *.o
